# muxmap

The muxmap package provides two types, `muxmap.Map` and `muxmap.RWMap`, which synchronize access to a map using a `sync.Mutex` and `sync.RWMutex` respectively.