package muxmap

import "sync"

// RWMap synchronized using a RWMutex. Use NewRW() to create a new instance.
type RWMap[K comparable, V any] struct {
	m map[K]V
	sync.RWMutex
}

// NewRWFromMap creates a new synchronized map from an existing map, "taking ownership" of the map.
func NewRWFromMap[K comparable, V any](m map[K]V) *RWMap[K, V] { return &RWMap[K, V]{m: m} }

// NewRW creates a new synchronized map
func NewRW[K comparable, V any]() *RWMap[K, V] { return &RWMap[K, V]{m: map[K]V{}} }

// Clone returns a copy of the map at the time of the call.
func (m *RWMap[K, V]) Clone() map[K]V {
	m.RLock()
	defer m.RUnlock()
	clone := map[K]V{}
	for k, v := range m.m {
		clone[k] = v
	}
	return clone
}

// Clear removes all elements from the map.
func (m *RWMap[K, V]) Clear() { m.Lock(); defer m.Unlock(); m.m = map[K]V{} }

func (m *RWMap[K, V]) Contains(k K) bool {
	m.RLock()
	defer m.RUnlock()
	_, ok := m.m[k]
	return ok
}

// Get returns the value for the given key, or the zero value if the key does not exist.
// It's the synchronized equivalent of the map access: v := m[k]
func (m *RWMap[K, V]) Get(k K) V { m.RLock(); defer m.RUnlock(); return m.m[k] }

// Get2 returns the value for the given key, and a boolean indicating whether the key exists.
// It's the synchronized equivalent of the two-value map access: v, ok := m[k]
func (m *RWMap[K, V]) Get2(k K) (V, bool) {
	m.RLock()
	defer m.RUnlock()
	v, ok := m.m[k]
	return v, ok
}

// Set sets the value for the given key. It's the synchronized equivalent of the map assignment: m[k] = v
func (m *RWMap[K, V]) Set(k K, v V) { m.Lock(); defer m.Unlock(); m.m[k] = v }

// Delete deletes the given key. It's the synchronized equivalent of the map deletion: delete(m, k)
func (m *RWMap[K, V]) Delete(k K) { m.Lock(); defer m.Unlock(); delete(m.m, k) }

// Update updates the value for the given key while holding the lock.
// This can be more efficient than Get/Set for a single key, but will only work properly if V is a reference type: use with care.
func (m *RWMap[K, V]) Update(k K, f func(v V)) { m.Lock(); defer m.Unlock(); f(m.m[k]) }

// Len returns the number of elements in the map.
func (m *RWMap[K, V]) Len() int { m.RLock(); defer m.RUnlock(); return len(m.m) }

// ReadRange calls the given function for each key/value pair in the map, while holding a read lock.
// It is YOUR responsibility to ensure that the function does not modify the map: use WriteRange for map modification.
// Use for map iteration. If f returns false, range stops the iteration.
func (m *RWMap[K, V]) ReadRange(f func(k K, v V) bool) {
	m.RLock()
	defer m.RUnlock()
	for k, v := range m.m {
		if !f(k, v) {
			return // stop iteration
		}
	}
}

// WriteRange calls the given function for each key/value pair in the map, while holding a write lock.
// Use for map iteration. If f returns false, range stops the iteration.
func (m *RWMap[K, V]) WriteRange(f func(k K, v V) bool) {
	m.Lock()
	defer m.Unlock()
	for k, v := range m.m {
		if !f(k, v) {
			return // stop iteration
		}
	}
}

// AppendKeys appends all keys in the map to the given slice and returns the result. No particular order is guaranteed.
// See also Keys(), AppendValues(), and AppendItems().
func (m *RWMap[K, V]) AppendKeys(keys []K) []K {
	m.RLock()
	defer m.RUnlock()
	for k := range m.m {
		keys = append(keys, k)
	}
	return keys
}

// Keys returns a slice containing all keys in the map. No particular order is guaranteed, even between calls.
// See also AppendKeys(), Values(), and Items().
func (m *RWMap[K, V]) Keys() []K { return m.AppendKeys(make([]K, 0, m.Len())) }

// AppendValues appends all values in the map to the given slice and returns the result. No particular order is guaranteed.
// See also Values(), AppendKeys(), and AppendItems().
func (m *RWMap[K, V]) AppendValues(vals []V) []V {
	m.RLock()
	defer m.RUnlock()
	for _, v := range m.m {
		vals = append(vals, v)
	}
	return vals
}

// Values returns a slice containing all values in the map. No particular order is guaranteed, even between calls.
// See also AppendValues(), Keys(), and Items().
func (m *RWMap[K, V]) Values() []V { return m.AppendValues(make([]V, 0, m.Len())) }

// AppendItems appends all key-value pairs in the map to the given slice and returns the result. No particular order is guaranteed.
// See also Items(), AppendKeys(), and AppendValues().
func (m *RWMap[K, V]) AppendItems(items []Item[K, V]) []Item[K, V] {
	m.RLock()
	defer m.RUnlock()
	for k, v := range m.m {
		items = append(items, Item[K, V]{k, v})
	}
	return items
}

// Items returns a slice containing all key-value pairs in the map. No particular order is guaranteed, even between calls.
// See also AppendItems(), Keys(), and Values().
func (m *RWMap[K, V]) Items() []Item[K, V] { return m.AppendItems(make([]Item[K, V], 0, m.Len())) }
