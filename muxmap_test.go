package muxmap_test

import (
	"slices"
	"sync"
	"testing"

	"gitlab.com/efronlicht/muxmap"
)

func TestMuxMap(t *testing.T) {
	m := muxmap.New[string, int]()
	wg := new(sync.WaitGroup)

	t.Run("set", func(t *testing.T) {
		wg.Add(26)
		for i := 'a'; i <= 'z'; i++ {
			go func(i rune) {
				m.Set(string(i), int(i))
				wg.Done()
			}(i)
		}
		wg.Wait()

		if m.Len() != 26 {
			t.Errorf("Expected m.Len() to be 26, got %d", m.Len())
		}
	})

	t.Run("get", func(t *testing.T) {
		wg.Add(26)
		for i := 'a'; i <= 'z'; i++ {
			go func(i rune) {
				if v := m.Get(string(i)); v != int(i) {
					t.Errorf("Expected %d, got %d", i, v)
				}
				wg.Done()
			}(i)
		}
		wg.Wait()
	})

	t.Run("keys, vals, items", func(t *testing.T) {
		keys := m.Keys()
		slices.Sort(keys)
		for i, k := range keys {
			if k[0] != 'a'+byte(i) {
				t.Errorf("Expected %c, got %s", 'a'+byte(i), k)
			}
		}

		values := m.Values()
		slices.Sort(values)

		for i, v := range values {
			if v != int('a'+i) {
				t.Errorf("Expected %d, got %d", 'a'+i, v)
			}
		}

		items := m.Items()
		slices.SortFunc(items, func(a, b muxmap.Item[string, int]) int {
			switch {
			case a.K < b.K:
				return -1
			case a.K > b.K:
				return 1
			case a.V < b.V:
				return -1
			case a.V > b.V:
				return 1
			default:
				return 0
			}
		})
		for i := range items {
			if items[i].K != keys[i] || items[i].V != values[i] {
				t.Errorf("Expected %s:%d, got %s:%d", keys[i], values[i], items[i].K, items[i].V)
			}
		}
	})

	m.Clear()

	wg.Add(26)
	for i := 'a'; i <= 'z'; i++ {
		go func(i rune) {
			if _, ok := m.Get2(string(i)); ok {
				t.Errorf("Expected %s to be deleted", string(i))
			}
			wg.Done()
		}(i)
	}
	wg.Wait()

	if m.Contains("foobar") {
		t.Errorf("Expected m.Contains(\"foobar\") to be false")
	}

	t.Run("range", func(t *testing.T) {
		evenKeys := make([]string, 0, 13)
		m.Range(func(k string, v int) bool {
			if v%2 == 0 {
				evenKeys = append(evenKeys, k)
			}
			return true
		})
		slices.Sort(evenKeys)
		for i, k := range evenKeys {
			if k[0] != 'a'+byte(i)*2 {
				t.Errorf("Expected %c, got %s", 'a'+byte(i)*2, k)
			}
		}
	})
}

func TestRWMap(t *testing.T) {
	m := muxmap.NewRW[string, int]()
	wg := new(sync.WaitGroup)

	t.Run("set", func(t *testing.T) {
		wg.Add(26)
		for i := 'a'; i <= 'z'; i++ {
			go func(i rune) {
				m.Set(string(i), int(i))
				wg.Done()
			}(i)
		}
		wg.Wait()

		if m.Len() != 26 {
			t.Errorf("Expected m.Len() to be 26, got %d", m.Len())
		}
	})

	t.Run("get", func(t *testing.T) {
		wg.Add(26)
		for i := 'a'; i <= 'z'; i++ {
			go func(i rune) {
				if v := m.Get(string(i)); v != int(i) {
					t.Errorf("Expected %d, got %d", i, v)
				}
				wg.Done()
			}(i)
		}
		wg.Wait()
	})

	t.Run("keys, vals, items", func(t *testing.T) {
		keys := m.Keys()
		slices.Sort(keys)
		for i, k := range keys {
			if k[0] != 'a'+byte(i) {
				t.Errorf("Expected %c, got %s", 'a'+byte(i), k)
			}
		}

		values := m.Values()
		slices.Sort(values)

		for i, v := range values {
			if v != int('a'+i) {
				t.Errorf("Expected %d, got %d", 'a'+i, v)
			}
		}

		items := m.Items()
		slices.SortFunc(items, func(a, b muxmap.Item[string, int]) int {
			switch {
			case a.K < b.K:
				return -1
			case a.K > b.K:
				return 1
			case a.V < b.V:
				return -1
			case a.V > b.V:
				return 1
			default:
				return 0
			}
		})
		for i := range items {
			if items[i].K != keys[i] || items[i].V != values[i] {
				t.Errorf("Expected %s:%d, got %s:%d", keys[i], values[i], items[i].K, items[i].V)
			}
		}
	})

	m.Clear()

	wg.Add(26)
	for i := 'a'; i <= 'z'; i++ {
		go func(i rune) {
			if _, ok := m.Get2(string(i)); ok {
				t.Errorf("Expected %s to be deleted", string(i))
			}
			wg.Done()
		}(i)
	}
	wg.Wait()

	if m.Contains("foobar") {
		t.Errorf("Expected m.Contains(\"foobar\") to be false")
	}

	t.Run("readrange", func(t *testing.T) {
		evenKeys := make([]string, 0, 13)
		m.ReadRange(func(k string, v int) bool {
			if v%2 == 0 {
				evenKeys = append(evenKeys, k)
			}
			return true
		})
		slices.Sort(evenKeys)
		for i, k := range evenKeys {
			if k[0] != 'a'+byte(i)*2 {
				t.Errorf("Expected %c, got %s", 'a'+byte(i)*2, k)
			}
		}
	})
	t.Run("writerange", func(t *testing.T) {
		pm := muxmap.NewRWFromMap(map[string]*int{
			"foo": new(int),
			"bar": new(int),
			"baz": new(int),
		})
		pm.WriteRange(func(k string, v *int) bool {
			*v = 42
			return true
		})
		pm.ReadRange(func(k string, v *int) bool {
			if *v != 42 {
				t.Errorf("Expected 42, got %d", v)
			}
			return true
		})
	})
}
