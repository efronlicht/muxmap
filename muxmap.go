// Package muxmap provides two synchronized map implementations: Map and RWMap, which
// use a sync.Mutex and sync.RWMutex respectively.
package muxmap

import "sync"

// Map synchronized using a mutex. Use New() or NewFromMap() to create a new instance.
type Map[K comparable, V any] struct {
	inner map[K]V    // underlying map
	mux   sync.Mutex // synchronize all accesses.
}

// New creates a new synchronized map ready for use..
func New[K comparable, V any]() *Map[K, V] { return &Map[K, V]{inner: map[K]V{}} }

// NewFromMap creates a new synchronized map from an existing map, "taking ownership" of the map.
// The original map should not be used after this call.
func NewFromMap[K comparable, V any](m map[K]V) *Map[K, V] { return &Map[K, V]{inner: m} }

// Clone returns a copy of the map at the time of the call.
func (m *Map[K, V]) Clone() map[K]V {
	m.mux.Lock()
	defer m.mux.Unlock()
	clone := map[K]V{}
	for k, v := range m.inner {
		clone[k] = v
	}
	return clone
}

// Get returns the value for the given key, or the zero value if the key does not exist.
// It's the synchronized equivalent of the map access: v := m[k]
// See Get2 for a way to distinguish between the two cases.
func (m *Map[K, V]) Get(k K) V { m.mux.Lock(); defer m.mux.Unlock(); return m.inner[k] }

// Get2 returns the value for the given key, and a boolean indicating whether the key exists.
// It's the synchronized equivalent of the two-value map access: v, ok := m[k]
func (m *Map[K, V]) Get2(k K) (V, bool) {
	m.mux.Lock()
	defer m.mux.Unlock()
	v, ok := m.inner[k]
	return v, ok
}

// Set sets the value for the given key. It's the synchronized equivalent of the map assignment: m[k] = v
func (m *Map[K, V]) Set(k K, v V) { m.mux.Lock(); defer m.mux.Unlock(); m.inner[k] = v }

// Delete deletes the given key. It's the synchronized equivalent of the map deletion: delete(m, k)
func (m *Map[K, V]) Delete(k K) { m.mux.Lock(); defer m.mux.Unlock(); delete(m.inner, k) }

// Len returns the number of elements in the map.
func (m *Map[K, V]) Len() int { m.mux.Lock(); defer m.mux.Unlock(); return len(m.inner) }

// Range calls f sequentially for each key and value present in the map, holding the mutex for the duration of the call.
// Use for map iteration. If f returns false, range stops the iteration.
// This is often significantly cheaper than calling Get() for each key.
func (m *Map[K, V]) Range(f func(k K, v V) bool) {
	m.mux.Lock()
	defer m.mux.Unlock()
	for k, v := range m.inner {
		if !f(k, v) {
			return
		}
	}
}

// AppendKeys appends all keys in the map to the given slice and returns the result. No particular order is guaranteed.
// See also Keys().
func (m *Map[K, V]) AppendKeys(keys []K) []K {
	m.mux.Lock()
	defer m.mux.Unlock()
	for k := range m.inner {
		keys = append(keys, k)
	}
	return keys
}

// Keys returns a slice containing all keys in the map. No particular order is guaranteed, even between calls.
// See also AppendKeys().
func (m *Map[K, V]) Keys() []K { return m.AppendKeys(make([]K, 0, m.Len())) }

func (m *Map[K, V]) AppendValues(vals []V) []V {
	m.mux.Lock()
	defer m.mux.Unlock()
	for _, v := range m.inner {
		vals = append(vals, v)
	}
	return vals
}

// Item is a key-value pair.
type Item[K, V any] struct {
	K K
	V V
}

// Values returns a slice containing all values in the map. No particular order is guaranteed, even between calls.
func (m *Map[K, V]) Values() []V { return m.AppendValues(make([]V, 0, m.Len())) }

func (m *Map[K, V]) AppendItems(items []Item[K, V]) []Item[K, V] {
	m.mux.Lock()
	defer m.mux.Unlock()
	for k, v := range m.inner {
		items = append(items, Item[K, V]{k, v})
	}
	return items
}

// Items returns a slice containing all key-value pairs in the map. No particular order is guaranteed, even between calls.
func (m *Map[K, V]) Items() []Item[K, V] { return m.AppendItems(make([]Item[K, V], 0, m.Len())) }

// Clear removes all elements from the map, as though calling clear() on the underlying map.
func (m *Map[K, V]) Clear() { m.mux.Lock(); defer m.mux.Unlock(); clear(m.inner) }

// Contains returns true if the given key exists in the map.
func (m *Map[K, V]) Contains(k K) bool {
	m.mux.Lock()
	defer m.mux.Unlock()
	_, ok := m.inner[k]
	return ok
}
